QT      += core network
CONFIG  += no_batch
DEFINES += QAPPLICATION_CLASS=QApplication
QMAKE_CXXFLAGS_WARN_ON += -wd4100

CONFIG(debug, debug|release) {
DESTDIR  = $$PWD/bin/Debug
} else {
DESTDIR  = $$PWD/bin/Release
DEFINES += QT_NO_DEBUG_OUTPUT
}

win32 {mkdestdir.commands = $(CHK_DIR_EXISTS) $$shell_path($$DESTDIR) $(MKDIR) $$shell_path($$DESTDIR)}
unix  {mkdestdir.commands = $(CHK_DIR_EXISTS) $$shell_path($$DESTDIR) & $(MKDIR) $$shell_path($$DESTDIR)}
QMAKE_EXTRA_TARGETS += mkdestdir
PRE_TARGETDEPS      += mkdestdir

# efsw
HEADERS += \
    $$PWD/efsw/include/efsw/efsw.h \
    $$PWD/efsw/include/efsw/efsw.hpp \
    $$PWD/efsw/src/efsw/platform/platformimpl.hpp \
    $$PWD/efsw/src/efsw/Atomic.hpp \
    $$PWD/efsw/src/efsw/Debug.hpp \
    $$PWD/efsw/src/efsw/DirWatcherGeneric.hpp \
    $$PWD/efsw/src/efsw/DirectorySnapshot.hpp \
    $$PWD/efsw/src/efsw/DirectorySnapshotDiff.hpp \
    $$PWD/efsw/src/efsw/FileInfo.hpp \
    $$PWD/efsw/src/efsw/FileSystem.hpp \
    $$PWD/efsw/src/efsw/FileWatcherGeneric.hpp \
    $$PWD/efsw/src/efsw/FileWatcherImpl.hpp \
    $$PWD/efsw/src/efsw/Lock.hpp \
    $$PWD/efsw/src/efsw/Mutex.hpp \
    $$PWD/efsw/src/efsw/String.hpp \
    $$PWD/efsw/src/efsw/System.hpp \
    $$PWD/efsw/src/efsw/Thread.hpp \
    $$PWD/efsw/src/efsw/Utf.hpp \
    $$PWD/efsw/src/efsw/Utf.inl \
    $$PWD/efsw/src/efsw/Watcher.hpp \
    $$PWD/efsw/src/efsw/WatcherGeneric.hpp \
    $$PWD/efsw/src/efsw/base.hpp \
    $$PWD/efsw/src/efsw/sophist.h
SOURCES += \
    $$PWD/efsw/src/efsw/Debug.cpp \
    $$PWD/efsw/src/efsw/DirWatcherGeneric.cpp \
    $$PWD/efsw/src/efsw/DirectorySnapshot.cpp \
    $$PWD/efsw/src/efsw/DirectorySnapshotDiff.cpp \
    $$PWD/efsw/src/efsw/FileInfo.cpp \
    $$PWD/efsw/src/efsw/FileSystem.cpp \
    $$PWD/efsw/src/efsw/FileWatcher.cpp \
    $$PWD/efsw/src/efsw/FileWatcherCWrapper.cpp \
    $$PWD/efsw/src/efsw/FileWatcherGeneric.cpp \
    $$PWD/efsw/src/efsw/FileWatcherImpl.cpp \
    $$PWD/efsw/src/efsw/Log.cpp \
    $$PWD/efsw/src/efsw/Mutex.cpp \
    $$PWD/efsw/src/efsw/String.cpp \
    $$PWD/efsw/src/efsw/System.cpp \
    $$PWD/efsw/src/efsw/Thread.cpp \
    $$PWD/efsw/src/efsw/Watcher.cpp \
    $$PWD/efsw/src/efsw/WatcherGeneric.cpp

win32 {
HEADERS += \
    $$PWD/efsw/src/efsw/platform/win/FileSystemImpl.hpp \
    $$PWD/efsw/src/efsw/platform/win/MutexImpl.hpp \
    $$PWD/efsw/src/efsw/platform/win/SystemImpl.hpp \
    $$PWD/efsw/src/efsw/platform/win/ThreadImpl.hpp
SOURCES += \
    $$PWD/efsw/src/efsw/platform/win/FileSystemImpl.cpp \
    $$PWD/efsw/src/efsw/platform/win/MutexImpl.cpp \
    $$PWD/efsw/src/efsw/platform/win/SystemImpl.cpp \
    $$PWD/efsw/src/efsw/platform/win/ThreadImpl.cpp
} else {
HEADERS += \
    $$PWD/efsw/src/efsw/platform/posix/FileSystemImpl.hpp \
    $$PWD/efsw/src/efsw/platform/posix/MutexImpl.hpp \
    $$PWD/efsw/src/efsw/platform/posix/SystemImpl.hpp \
    $$PWD/efsw/src/efsw/platform/posix/ThreadImpl.hpp
SOURCES += \
    $$PWD/efsw/src/efsw/platform/posix/FileSystemImpl.cpp \
    $$PWD/efsw/src/efsw/platform/posix/MutexImpl.cpp \
    $$PWD/efsw/src/efsw/platform/posix/SystemImpl.cpp \
    $$PWD/efsw/src/efsw/platform/posix/ThreadImpl.cpp
}

win32 {
HEADERS += \
    $$PWD/efsw/src/efsw/FileWatcherWin32.hpp \
    $$PWD/efsw/src/efsw/WatcherWin32.hpp
SOURCES += \
    $$PWD/efsw/src/efsw/FileWatcherWin32.cpp \
    $$PWD/efsw/src/efsw/WatcherWin32.cpp
}

macx {
HEADERS += \
    $$PWD/efsw/src/efsw/FileWatcherFSEvents.hpp \
    $$PWD/efsw/src/efsw/FileWatcherKqueue.hpp \
    $$PWD/efsw/src/efsw/WatcherFSEvents.hpp \
    $$PWD/efsw/src/efsw/WatcherKqueue.hpp
SOURCES += \
    $$PWD/efsw/src/efsw/FileWatcherFSEvents.cpp \
    $$PWD/efsw/src/efsw/FileWatcherKqueue.cpp \
    $$PWD/efsw/src/efsw/WatcherFSEvents.cpp \
    $$PWD/efsw/src/efsw/WatcherKqueue.cpp
}

unix:!macx {
HEADERS += \
    $$PWD/efsw/src/efsw/FileWatcherInotify.hpp \
    $$PWD/efsw/src/efsw/WatcherInotify.hpp \
    $$PWD/efsw/src/efsw/inotify-nosys.h
SOURCES += \
    $$PWD/efsw/src/efsw/FileWatcherInotify.cpp \
    $$PWD/efsw/src/efsw/WatcherInotify.cpp
}

bsd|freebsd {
HEADERS += \
    $$PWD/efsw/src/efsw/FileWatcherKqueue.hpp \
    $$PWD/efsw/src/efsw/WatcherKqueue.hpp
SOURCES += \
    $$PWD/efsw/src/efsw/FileWatcherKqueue.cpp \
    $$PWD/efsw/src/efsw/WatcherKqueue.cpp
}

INCLUDEPATH += $$PWD/efsw/include $$PWD/efsw/src
DEPENDPATH  += $$PWD/efsw/include $$PWD/efsw/src

# singleapplication
HEADERS += \
    $$PWD/singleapplication/SingleApplication \
    $$PWD/singleapplication/singleapplication.h \
    $$PWD/singleapplication/singleapplication_p.h
SOURCES += \
    $$PWD/singleapplication/singleapplication.cpp \
    $$PWD/singleapplication/singleapplication_p.cpp

INCLUDEPATH += $$PWD/singleapplication
DEPENDPATH  += $$PWD/singleapplication
