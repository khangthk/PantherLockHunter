QT      += core gui svg widgets

CONFIG  += c++17

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

include(PantherLockHunter.pri)

VERSION = 1.0.0.0
RC_ICONS = icons/app.ico
QMAKE_TARGET_COMPANY = "Freeware"
QMAKE_TARGET_PRODUCT = "Panther Lock Hunter"
QMAKE_TARGET_DESCRIPTION = "Panther Lock Hunter"
QMAKE_TARGET_COPYRIGHT = "Copyright \\251 2023"

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    pantherlistener.cpp \
    pantherwatcher.cpp \
    scanlockfile.cpp \
    setting.cpp \
    systemtray.cpp \
    tababout.cpp \
    tablog.cpp \
    tabmain.cpp \
    tabsetting.cpp

HEADERS += \
    mainwindow.h \
    pantherlistener.h \
    pantherwatcher.h \
    scanlockfile.h \
    setting.h \
    systemtray.h \
    tababout.h \
    tablog.h \
    tabmain.h \
    tabsetting.h

FORMS   += \
    mainwindow.ui \
    tababout.ui \
    tablog.ui \
    tabmain.ui \
    tabsetting.ui

RESOURCES += \
    PantherLockHunter.qrc
