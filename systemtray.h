#pragma once

#include <QObject>
#include <QAction>
#include <QSystemTrayIcon>

class MainWindow;

class SystemTray : public QSystemTrayIcon
{
    Q_OBJECT
public:
    SystemTray(MainWindow *parent = nullptr);
    void showMessageRunningInSystemTray();
    void showMessageAlreadyRunning();
    void updateMenu();

private:
    QAction *m_openAction;
    QAction *m_quitAction;
    QMenu *m_trayIconMenu;
    MainWindow *m_parent;

private slots:
    void onOpen();
    void onActivated(QSystemTrayIcon::ActivationReason reason);
};
