#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tabmain.h"
#include "tablog.h"
#include "tabsetting.h"
#include "setting.h"

#include <QDebug>
#include <QPainter>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(ui->tabParent);

    m_watcher = new PantherWatcher();
    m_systemTray = new SystemTray(this);

    initStatusBar();

    connect(this, &MainWindow::addLog, ui->tabLog, &TabLog::onAddLog);
    connect(this, &MainWindow::startStopHunter, ui->tabMain, &TabMain::onUpdateButtonStart);
    connect(this, &MainWindow::startStopHunter, this, &MainWindow::onUpdateHunterStatus);

    connect(m_watcher, &PantherWatcher::fileDeleted, this, &MainWindow::onFileDeleted);

    connect(ui->tabMain, &TabMain::addWatch, this, &MainWindow::onAddWatch);
    connect(ui->tabMain, &TabMain::deleteWatch, this, &MainWindow::onDeleteWatch);
    connect(ui->tabMain, &TabMain::startWatcher, this, &MainWindow::onStartWatcher);
    connect(ui->tabMain, &TabMain::pantherSiteListChanged, this, &MainWindow::onUpdateHunterStatus);
    connect(ui->tabMain, &TabMain::startScan, this, &MainWindow::onScanStarted);
    connect(ui->tabMain, &TabMain::scanDone, this, &MainWindow::onScanDone);

    connect(ui->tabSetting, &TabSetting::moveLockFileToTrash, m_watcher, &PantherWatcher::onUpdateMoveToTrash);
    connect(ui->tabSetting, &TabSetting::updateTimerClearLog, ui->tabLog, &TabLog::onUpdateTimerClearLog);
    connect(ui->tabSetting, &TabSetting::lockFileListChanged, this, &MainWindow::onUpdateHunterStatus);
    connect(ui->tabSetting, &TabSetting::lockFileListChanged, m_watcher, &PantherWatcher::onUpdateLockFileList);
    connect(ui->tabSetting, &TabSetting::lockFileListChanged, ui->tabMain, &TabMain::onUpdateButtonScan);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::starHunter(const bool autoStart)
{
    if (autoStart)
    {
        onStartWatcher();
    }

    emit startStopHunter(m_watcher->running());
    ui->tabLog->onUpdateTimerClearLog();
}

void MainWindow::showEvent(QShowEvent *event)
{
    QMainWindow::showEvent(event);

    onUpdateHunterStatus();
    onUpdateTotalDeletedFiles();
    onUpdateScanProgressBar();

    if (m_systemTray != nullptr) {
        m_systemTray->updateMenu();
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    hide();
    event->ignore();
    if (m_systemTray != nullptr) {
        m_systemTray->showMessageRunningInSystemTray();
        m_systemTray->updateMenu();
    }
}

void MainWindow::initStatusBar()
{
    m_statusIcon = new QLabel(ui->statusbar);
    m_statusText = new QLabel(ui->statusbar);
    m_totalFile = new QLabel(ui->statusbar);
    m_dummy = new QLabel(ui->statusbar);
    m_progressBar = new QProgressBar(ui->statusbar);

    m_progressBar->setRange(0, 0);
    m_progressBar->setFixedHeight(15);
    m_progressBar->setTextVisible(false);

    ui->statusbar->addPermanentWidget(m_statusIcon, 0);
    ui->statusbar->addPermanentWidget(m_statusText, 1);
    ui->statusbar->addPermanentWidget(m_totalFile, 1);
    ui->statusbar->addPermanentWidget(m_dummy, 1);
    ui->statusbar->addPermanentWidget(m_progressBar, 1);
}

void MainWindow::onInstanceAlreadyExists(quint32 instanceId, const QByteArray &message)
{
    qDebug() << instanceId << message;
    if (m_systemTray != nullptr)
    {
        m_systemTray->showMessageAlreadyRunning();
        m_systemTray->updateMenu();
    }
}

void MainWindow::onAddWatch(const QString &dirPath)
{
    if (m_watcher->running())
    {
        m_watcher->addDir(dirPath);
    }
}

void MainWindow::onDeleteWatch(const QStringList &dirPaths)
{
    if (m_watcher->running())
    {
        foreach (auto &dir, dirPaths)
        {
            m_watcher->removeDir(dir);
        }
    }
}

void MainWindow::onStartWatcher()
{
    if (m_watcher->running())
    {
        m_watcher->stop();
        qDebug() << "Stop Hunter";
    }
    else
    {
        m_watcher->start();
        qDebug() << "Start Hunter";

        if (m_watcher->running())
        {
            QStringList dirPaths = Setting::getWatchFolders();
            foreach (auto &dir, dirPaths)
            {
                if (m_watcher->running())
                {
                    m_watcher->addDir(dir);
                }
            }
        }
    }

    emit startStopHunter(m_watcher->running());
}

void MainWindow::onFileDeleted(const QString &filePath, const bool success)
{
    QString log = "Found: " + filePath;
    emit addLog(log);
    qDebug() << log;
    log = success ? "->Deleted successfully!" : "->Delete failed!";
    emit addLog(log);
    qDebug() << log;

    // Update total deleted files
    m_totalFile->setText("Deleted Files: " + QString::number(m_watcher->totalDeletedFiles()));
}

void MainWindow::onUpdateHunterStatus()
{
    QColor iconColor = QColor(107, 190, 102); //green

    if (m_watcher->running())
    {
        if (Setting::getWatchFolders().empty() || Setting::getLockFiles().empty())
        {
            iconColor = QColor(255, 160, 0); //yellow
        }

        m_statusText->setText("Hunting");

        auto fnSetAnimationText = [&]()
        {
            static int i = 0;
            QString text = "Hunting";
            switch (i++) {
            case 0:
                text += " \\";
                break;
            case 1:
                text += " |";
                break;
            case 2:
                text += " /";
                break;
            default:
                text += " --";
                i = 0;
                break;
            }
            m_statusText->setText(text);
        };

        if (!m_timer.isActive())
        {
            connect(&m_timer, &QTimer::timeout, this, fnSetAnimationText);
            m_timer.start(130);
        }
    }
    else
    {
        m_timer.stop();
        disconnect(&m_timer, &QTimer::timeout, 0, 0);
        m_statusText->setText("Stop");
        iconColor = QColor(251, 0, 0); //red
    }

    QPixmap iconPixmap(ui->statusbar->height() / 2, ui->statusbar->height() / 2);
    iconPixmap.fill(Qt::transparent);
    QPainter painter(&iconPixmap);
    painter.setBrush(iconColor);
    painter.setPen(Qt::transparent);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.drawEllipse(iconPixmap.rect());
    m_statusIcon->setPixmap(iconPixmap);
}

void MainWindow::onUpdateTotalDeletedFiles()
{
    m_totalFile->setText("Deleted Files: " + QString::number(m_watcher->totalDeletedFiles()));
}

void MainWindow::onUpdateScanProgressBar()
{
    if (ui->tabMain->scanning())
    {
        m_progressBar->show();
        m_dummy->hide();
    }
    else
    {
        m_progressBar->hide();
        m_dummy->show();
    }
}

void MainWindow::onScanStarted()
{
    onUpdateScanProgressBar();
}

void MainWindow::onScanDone(const int numberDeletedFiles, const QStringList &details)
{
    if (!isVisible())
    {
        showNormal();
    }

    onUpdateScanProgressBar();

    if (details.empty())
    {
        QMessageBox::information(this, "Scan Lock File", "Lock file not found.");
    }
    else
    {
        QMessageBox msgBox(this);
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setWindowTitle("Scan Lock File");
        if (numberDeletedFiles == details.count() / 2)
        {
            msgBox.setText(QString("Found and deleted %1 lock file%2.                                                             ")
                                   .arg(numberDeletedFiles).arg(numberDeletedFiles > 1 ? "s" : ""));
        }
        else
        {
            msgBox.setText(QString("Deleted %1/%2 lock file%3.                                                                    ")
                                   .arg(numberDeletedFiles).arg(details.count() / 2).arg(numberDeletedFiles > 1 ? "s" : ""));
        }

        QString detailText;
        foreach (auto &detail, details)
        {
            detailText += detail + "\n";
        }
        msgBox.setDetailedText(detailText);
        msgBox.exec();
    }
}
