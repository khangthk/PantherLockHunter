#include "mainwindow.h"
#include "setting.h"

#include <SingleApplication>

int main(int argc, char *argv[])
{
    SingleApplication app(argc, argv, true);
    if (app.isSecondary())
    {
        app.sendMessage("Secondary Instance");
        return 0;
    }

    app.setOrganizationName("Freeware");
    app.setApplicationName("PantherLockHunter");

    MainWindow w;
    Setting::getHideWindowAtLaunch() ? w.hide() : w.show();
    w.starHunter(Setting::getAutoStartHunter());
    QObject::connect(&app, &SingleApplication::receivedMessage, &w, &MainWindow::onInstanceAlreadyExists);

    return app.exec();
}
