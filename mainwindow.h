#pragma once

#include "pantherwatcher.h"
#include "systemtray.h"

#include <QMainWindow>
#include <QProgressBar>
#include <QLabel>
#include <QTimer>
#include <QShowEvent>
#include <QCloseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void starHunter(const bool autoStart);

protected:
    void showEvent(QShowEvent *event) override;
    void closeEvent(QCloseEvent *event) override;

private:
    void initStatusBar();

signals:
    void addLog(const QString &log);
    void startStopHunter(const bool started);

public slots:
    void onInstanceAlreadyExists(quint32 instanceId, const QByteArray &message);

private slots:
    void onAddWatch(const QString &dirPath);
    void onDeleteWatch(const QStringList &dirPaths);
    void onStartWatcher();
    void onFileDeleted(const QString &filePath, const bool success);
    void onUpdateHunterStatus();
    void onUpdateTotalDeletedFiles();
    void onUpdateScanProgressBar();
    void onScanStarted();
    void onScanDone(const int numberDeletedFiles, const QStringList &details);

private:
    Ui::MainWindow *ui;
    QLabel *m_statusIcon;
    QLabel *m_statusText;
    QLabel *m_totalFile;
    QLabel *m_dummy;
    QProgressBar *m_progressBar;
    QTimer m_timer;
    PantherWatcher *m_watcher;
    SystemTray *m_systemTray;
};
