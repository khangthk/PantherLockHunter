#include "tabmain.h"
#include "ui_tabmain.h"
#include "setting.h"

#include <QFileDialog>

TabMain::TabMain(QWidget *parent) :
    QWidget(parent), ui(new Ui::TabMain), m_scanning(false), m_scanLockFile(nullptr)
{
    ui->setupUi(this);
    ui->listWidget->addItems(Setting::getWatchFolders());

    connect(ui->buttonAdd, &QAbstractButton::clicked, this, &TabMain::onAdd);
    connect(ui->buttonDelete, &QAbstractButton::clicked, this, &TabMain::onDelete);
    connect(ui->buttonStart, &QAbstractButton::clicked, this, &TabMain::onStart);
    connect(ui->buttonScan, &QAbstractButton::clicked, this, &TabMain::onScan);
    connect(ui->listWidget, &QListWidget::itemSelectionChanged, this, &TabMain::onPantherSiteListIndexChanged);
}

TabMain::~TabMain()
{
    delete ui;
}

bool TabMain::scanning() const
{
    return m_scanning;
}

void TabMain::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    onPantherSiteListIndexChanged();
}

void TabMain::updateButtonStart(const bool started)
{
    if (started)
    {
        ui->buttonStart->setText("Stop");
        ui->buttonStart->setToolTip("Stop Hunter");
    }
    else
    {
        ui->buttonStart->setText("Start");
        ui->buttonStart->setToolTip("Start Hunter");
    }
}

void TabMain::updateButtonDelete()
{
    ui->buttonDelete->setEnabled(!ui->listWidget->selectedItems().empty());
}

void TabMain::updateButtonScan()
{
    ui->buttonScan->setEnabled(!Setting::getWatchFolders().empty() && !Setting::getLockFiles().empty());
    if (m_scanning)
    {
        ui->buttonScan->setText("Cancel");
        ui->buttonScan->setToolTip("Cancel scanning");
    }
    else
    {
        ui->buttonScan->setText("Scan");
        ui->buttonScan->setToolTip("Scan and delete the lock files in Panther sites");
    }
}

void TabMain::onUpdateButtonStart(const bool started)
{
    updateButtonStart(started);
}

void TabMain::onUpdateButtonScan()
{
    updateButtonScan();
}

void TabMain::onAdd()
{
    QString lastPath = Setting::getLastPath();
    QString dir = QFileDialog::getExistingDirectory(this, "Select Panther Site", lastPath,
                                                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if (!dir.isEmpty())
    {
        Setting::setLastPath(dir);
        dir = QDir::toNativeSeparators(dir);
        if (ui->listWidget->findItems(dir, Qt::MatchFixedString).empty())
        {
            ui->listWidget->addItem(dir);
            // Update the watch folder list
            QStringList folders;
            for (int i = 0; i < ui->listWidget->count(); i++)
            {
                folders << ui->listWidget->item(i)->text();
            }
            Setting::setWatchFolders(folders);

            emit addWatch(dir);
            emit pantherSiteListChanged();
        }
    }

    onPantherSiteListIndexChanged();
}

void TabMain::onDelete()
{
    auto items = ui->listWidget->selectedItems();
    QStringList listDir;
    foreach (auto item, items)
    {
        listDir.append(item->text() + QDir::separator());
        delete ui->listWidget->takeItem(ui->listWidget->row(item));
    }

    if (!listDir.empty())
    {
        // Update the watch folder list
        QStringList folders;
        for (int i = 0; i < ui->listWidget->count(); i++)
        {
            folders << ui->listWidget->item(i)->text();
        }
        Setting::setWatchFolders(folders);

        emit deleteWatch(listDir);
        emit pantherSiteListChanged();
    }

    onPantherSiteListIndexChanged();
}

void TabMain::onStart()
{
    emit startWatcher();
}

void TabMain::onScan()
{
    if (!m_scanning)
    {
        m_scanning = true;
        m_scanLockFile = new ScanLockFile(this);

        connect(m_scanLockFile, &ScanLockFile::scanDone, this, [&](int numberDeletedFiles, const QStringList &filePaths)
        {
            m_scanning = false;
            updateButtonScan();
            emit scanDone(numberDeletedFiles, filePaths);
        });
        connect(m_scanLockFile, &ScanLockFile::finished, m_scanLockFile, &QObject::deleteLater);

        m_scanLockFile->start();
        emit startScan();
    }
    else
    {
        m_scanning = false;
        m_scanLockFile->cancel();
        m_scanLockFile->wait();
    }

    updateButtonScan();
}

void TabMain::onPantherSiteListIndexChanged()
{
    updateButtonDelete();
    updateButtonScan();
}
