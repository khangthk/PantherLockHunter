#include "scanlockfile.h"
#include "setting.h"

#include <QFile>
#include <QDebug>
#include <QDirIterator>

ScanLockFile::ScanLockFile(QObject *parent)
    : QThread(parent), m_numberDeletedFiles(0), m_canceled(false)
{
}

void ScanLockFile::cancel()
{
    m_mutex.lock();
    m_canceled = true;
    m_mutex.unlock();
}

bool ScanLockFile::deleteLockFile(const QString &filePath, const bool moveToTrash)
{
    QFile file(filePath);
    if (file.exists()) {
        bool success = true;
        if (moveToTrash)
        {
            if (file.moveToTrash() != success)
            {
                QThread::msleep(100);
                success = QFile::moveToTrash(filePath);
            }
        }
        else
        {
            if (file.remove() != success)
            {
                QThread::msleep(100);
                success = QFile::remove(filePath);
            }
        }
        return success;
    }
    return false;
}

void ScanLockFile::run()
{
    m_numberDeletedFiles = 0;
    QStringList dirList = Setting::getWatchFolders();
    QStringList lockFileList = Setting::getLockFiles();
    bool isMoveToTrash = Setting::getMoveToTrash();

    QElapsedTimer timer;
    timer.start();

    QStringList details;
    foreach (auto &dir, dirList)
    {
        QDirIterator it(dir, lockFileList, QDir::Files | QDir::System | QDir::NoDotAndDotDot | QDir::NoSymLinks , QDirIterator::Subdirectories);
        while (!m_canceled && it.hasNext())
        {
            QString filePath = it.next();
            bool success = deleteLockFile(filePath, isMoveToTrash);
            success ? ++m_numberDeletedFiles : false;
            filePath = QDir::toNativeSeparators(filePath);
            qDebug() << "Found: " + filePath;
            details << "Found: " + filePath;
            qDebug() << (success ? "->Deleted successfully!" : "->Delete failed!");
            details << (success ? "->Deleted successfully!" : "->Delete failed!");
        }

        if (m_canceled)
        {
            qDebug() << "Cancel scan lock file";
            break;
        }
    }

    qDebug() << "Scan done";
    emit scanDone(m_numberDeletedFiles, details);

    qDebug() << "Scan time: " << timer.elapsed() / 1000 << "s";
}
