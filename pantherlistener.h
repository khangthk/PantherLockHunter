#pragma once

#include <efsw/efsw.hpp>
#include <QObject>

class PantherListener : public QObject, public efsw::FileWatchListener
{
    Q_OBJECT
public:
    explicit PantherListener(QObject *parent = nullptr);
    void handleFileAction(efsw::WatchID watchID, const std::string &dir, const std::string &filename,
                          efsw::Action action, std::string oldFilename = "") override;

    void setLockFiles(const QStringList &lockFiles);
    void setMoveToTrash(const bool moveToTrash);

signals:
    void fileDeleted(const QString &filePath, const bool success);

private:
    QStringList m_lockFiles;
    bool m_moveToTrash;
};

