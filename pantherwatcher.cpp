#include "pantherwatcher.h"
#include "setting.h"

#include <efsw/base.hpp>
#include <QDebug>

PantherWatcher::PantherWatcher(QObject *parent)
    : QObject(parent), m_watcher(nullptr), m_listener(nullptr), m_totalDeletedFiles(0)
{
}

PantherWatcher::~PantherWatcher()
{
    stop();
}

void PantherWatcher::start()
{
    m_watcher = new efsw::FileWatcher();
    m_listener = new PantherListener(this);
    m_listener->setLockFiles(Setting::getLockFiles());
    m_listener->setMoveToTrash(Setting::getMoveToTrash());
    m_watcher->watch();

    connect(m_listener, &PantherListener::fileDeleted, this, [&](const QString &filePath, bool success)
    {
        if (success)
        {
            ++m_totalDeletedFiles;
            qDebug() << "Total file deleted: " << m_totalDeletedFiles;
        }

        emit fileDeleted(filePath, success);
    });
}

void PantherWatcher::stop()
{
    efSAFE_DELETE(m_listener);
    efSAFE_DELETE(m_watcher);
}

bool PantherWatcher::addDir(const QString &dirPath)
{
    if (running() && m_watcher->addWatch(dirPath.toStdString(), m_listener, true) > 0)
    {
        qDebug() << "Watching directory: " << dirPath;
    }
    else
    {
        qDebug() << "Error trying to watch directory: " << dirPath;
        qDebug() << efsw::Errors::Log::getLastErrorLog().c_str();
        return false;
    }

    return true;
}

bool PantherWatcher::removeDir(const QString &dirPath)
{
    if (running())
    {
        m_watcher->removeWatch(dirPath.toStdString());
        return true;
    }

    return false;
}

bool PantherWatcher::running() const
{
    return m_watcher != nullptr && m_listener != nullptr;
}

int PantherWatcher::totalDeletedFiles()
{
    return m_totalDeletedFiles;
}

void PantherWatcher::onUpdateLockFileList()
{
    if (running())
    {
        m_listener->setLockFiles(Setting::getLockFiles());
    }
}

void PantherWatcher::onUpdateMoveToTrash()
{
    if (running())
    {
        m_listener->setMoveToTrash(Setting::getMoveToTrash());
    }
}
