#pragma once

#include "pantherlistener.h"

#include <QObject>

class PantherWatcher : public QObject
{
    Q_OBJECT
public:
    explicit PantherWatcher(QObject *parent = nullptr);
    virtual ~PantherWatcher();

    void start();
    void stop();
    bool addDir(const QString &dirPath);
    bool removeDir(const QString &dirPath);
    bool running() const;
    int totalDeletedFiles();

signals:
    void fileDeleted(const QString &filePath, const bool success);

public slots:
    void onUpdateLockFileList();
    void onUpdateMoveToTrash();

private:
    efsw::FileWatcher *m_watcher;
    PantherListener *m_listener;
    int m_totalDeletedFiles;
};

