#pragma once

#include "scanlockfile.h"

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class TabMain; }
QT_END_NAMESPACE

class TabMain : public QWidget
{
    Q_OBJECT

public:
    explicit TabMain(QWidget *parent = nullptr);
    ~TabMain();
    bool scanning() const;

protected:
    void showEvent(QShowEvent *event) override;

private:
    void updateButtonStart(const bool started);
    void updateButtonDelete();
    void updateButtonScan();

signals:
    void addWatch(const QString &dirPath);
    void deleteWatch(const QStringList &dirPaths);
    void startWatcher();
    void pantherSiteListChanged();
    void startScan();
    void scanDone(const int numberDeletedFiles, const QStringList &filePaths);

public slots:
    void onUpdateButtonStart(const bool started);
    void onUpdateButtonScan();

private slots:
    void onAdd();
    void onDelete();
    void onStart();
    void onScan();
    void onPantherSiteListIndexChanged();

private:
    Ui::TabMain *ui;
    bool m_scanning;
    ScanLockFile *m_scanLockFile;
};
