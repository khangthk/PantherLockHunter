#include "pantherlistener.h"

#include <QFile>
#include <QDir>
#include <QThread>

PantherListener::PantherListener(QObject *parent) : QObject(parent)
{
}

void PantherListener::handleFileAction(efsw::WatchID watchID, const std::string &dir, const std::string &filename,
                                       efsw::Action action, std::string oldFilename)
{
    Q_UNUSED(watchID)
    Q_UNUSED(oldFilename)

    QString qdir = QDir::toNativeSeparators(QString::fromStdString(dir));
    QString qfilename = QString::fromStdString(filename);

    if (m_lockFiles.contains(qfilename, Qt::CaseInsensitive) &&
       (action == efsw::Actions::Add || action == efsw::Actions::Modified))
    {
        if (qdir.lastIndexOf(QDir::separator()) != qdir.length() - 1)
        {
            qdir += QDir::separator();
        }

        QString path = qdir + qfilename;
        QFile file(path);

        if (file.exists())
        {
            bool success = true;
            if (m_moveToTrash)
            {
                if (!file.moveToTrash())
                {
                    QThread::msleep(100);
                    success = QFile::moveToTrash(path);
                }
            }
            else
            {
                if (!file.remove())
                {
                    QThread::msleep(100);
                    success = QFile::remove(path);
                }
            }

            emit fileDeleted(path, success);
        }
    }
}

void PantherListener::setLockFiles(const QStringList &lockFiles)
{
    m_lockFiles = lockFiles;
}

void PantherListener::setMoveToTrash(const bool moveToTrash)
{
    m_moveToTrash = moveToTrash;
}
