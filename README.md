# PantherLockHunter
Automatically delete the lock file when running TPS

![](/images/Main.png)
![](/images/Log.png)
![](/images/Setting.png)
![](/images/About.png)
![](/images/Scan.png)
