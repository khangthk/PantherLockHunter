#include "systemtray.h"
#include "mainwindow.h"

#include <QApplication>
#include <QMenu>

SystemTray::SystemTray(MainWindow *parent)
    : QSystemTrayIcon(parent), m_parent(parent)
{
    Q_ASSERT(parent != nullptr);

    m_openAction = new QAction("&Show", parent);
    m_quitAction = new QAction("&Quit", parent);
    m_trayIconMenu = new QMenu(parent);

    m_trayIconMenu->addAction(m_openAction);
    m_trayIconMenu->addAction(m_quitAction);

    setContextMenu(m_trayIconMenu);
    setIcon(QIcon(":/icons/app.svg"));
    setToolTip("Panther Lock Hunter");
    setVisible(true);

    connect(m_openAction, &QAction::triggered, this, &SystemTray::onOpen);
    connect(m_quitAction, &QAction::triggered, qApp, &QCoreApplication::exit);
    connect(this, &QSystemTrayIcon::activated, this, &SystemTray::onActivated);
}

void SystemTray::showMessageRunningInSystemTray()
{
    QString title = "Panther Lock Hunter";
    QString body = "The program will keep running in the system tray.";
    showMessage(title, body);
}

void SystemTray::showMessageAlreadyRunning()
{
    QString title = "Panther Lock Hunter";
    QString body = "An instance of the application is already running.";
    showMessage(title, body);
}

void SystemTray::updateMenu()
{
    if (m_parent->isVisible())
    {
        m_openAction->setText("&Hide");
    }
    else
    {
        m_openAction->setText("&Show");
    }
}

void SystemTray::onOpen()
{
    if (m_openAction->text() == "&Show")
    {
        m_parent->showNormal();
        m_parent->activateWindow();
    }
    else if (m_openAction->text() == "&Hide")
    {
        m_parent->hide();
    }

    updateMenu();
}

void SystemTray::onActivated(ActivationReason reason)
{
    switch (reason)
    {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        m_parent->showNormal();
        m_parent->activateWindow();
        m_openAction->setText("&Hide");
        break;
    case QSystemTrayIcon::MiddleClick:
        break;
    default:
        break;
    }
}
