#pragma once

#include <QMutex>
#include <QThread>

class ScanLockFile : public QThread
{
    Q_OBJECT
public:
    explicit ScanLockFile(QObject *parent = nullptr);
    void cancel();

private:
    bool deleteLockFile(const QString &filePath, const bool moveToTrash);
    void run() override;

signals:
    void scanDone(const int numberDeletedFiles, const QStringList &details);

private:
    int m_numberDeletedFiles;
    QMutex m_mutex;
    bool m_canceled;
};
