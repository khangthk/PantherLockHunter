#pragma once

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class TabSetting; }
QT_END_NAMESPACE

class TabSetting : public QWidget
{
    Q_OBJECT

public:
    explicit TabSetting(QWidget *parent = nullptr);
    ~TabSetting();

protected:
    void showEvent(QShowEvent *event) override;

private:
    void initData();
    void updateButton();

signals:
    void moveLockFileToTrash();
    void updateTimerClearLog();
    void lockFileListChanged();

private slots:
    void onRunOnSystemStartup(const int state);
    void onHideMainWindowWhenOpen(const int state);
    void onAutoStartHunter(const int state);
    void onMoveToTrash(const int state);
    void onAutoClearLog(const int state);
    void onHoursIndexChanged(const int index);
    void onLockFileListIndexChanged();
    void onAdd();
    void onDelete();
    void onDefault();

private:
    Ui::TabSetting *ui;
};
